<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/39939442/doctolib_logo.png" height=100 />
</p>

<h1 align=center>Doctolib Crawler</h1>

<br/>
This crawler checks the Doctolib API for available appointments at regular intervals.<br/>
This automation saves time by keeping an eye on open slots, making it easier for users to find and book appointments without constant manual searches.<br/>

## Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)


<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):
```
composer config repositories.get-repo/doctolib-crawler git https://gitlab.com/get-repo/doctolib-crawler.git
composer require get-repo/doctolib-crawler
cd ./doctolib-crawler
composer install
// will ask notitication parameters
```

<br/>

## Usage

**The Doctolib API looks like this :**
```
https://www.doctolib.fr/availabilities.json?visit_motive_ids=1111&agenda_ids=2222&practice_ids=3333&telehealth=false&&start_date=2024-01-01
```
**Note the following options :**
- visit_motive_ids = 1111
- agenda_ids = 2222
- practice_ids = 3333

**Run the following command with arguments that matches the API :**
`php doctolib 1111 2222 3333`
```
Usage:
  doctolib [options] [--] <visit-motive-id> <agenda-id> <practice-id>

Arguments:
  visit-motive-id       visit_motive_ids GET parameter
  agenda-id             agenda_ids GET parameter
  practice-id           practice_ids GET parameter

Options:
      --name=NAME       name
      --days=DAYS       days (default 30 days) [default: 30]
```
