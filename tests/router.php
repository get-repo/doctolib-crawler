<?php

$startDate = $_GET['start_date'] ?? false;
$visitMotiveId = $_GET['visit_motive_ids'] ?? false;
$agendaId = $_GET['agenda_ids'] ?? false;
$practiceId = $_GET['practice_ids'] ?? false;

if ($startDate && $visitMotiveId && $agendaId && $practiceId) {
    if ($visitMotiveId == $agendaId && $agendaId == $practiceId) {
        echo '["INVALID_JSON_FORMAT"]';
        exit;
    }

    $data = [];

    $start = new DateTime($startDate);
    $interval = new DateInterval('P1D');
    $end = new DateTime("{$startDate} +2 days");
    $period = new DatePeriod($start, $interval, $end);

    $total = 0;
    foreach ($period as $date) {
        $availability = [
            'date' => $dateString = $date->format('Y-m-d'),
            'slots' => [],
        ];
        if (!in_array($visitMotiveId, [8888, 9999])) {
            $availability['slots'][] = sprintf(
                '%sT%02d:00:00.000+01:00',
                $dateString,
                ($date->format('d') % 12)
            );
            $availability['slots'][] = sprintf(
                '%sT%02d:30:00.000+01:00',
                $dateString,
                (($date->format('d') + 2) % 23)
            );
        }
        $total += count($availability['slots']);
        $data['availabilities'][] = $availability;
    }
    $data['total'] = $total;

    if (9999 == $visitMotiveId) {
        $data['reason'] = 'no_availabilities';
        $data['message'] = 'Aucune disponibilité en ligne.';
    }

    if (8888 == $visitMotiveId) {
        $data['reason'] = 'not_opened_availability';
        $data['message'] = 'Ces dates ne sont pas encore ouvertes à la réservation.';
    }

    echo json_encode($data, JSON_THROW_ON_ERROR);
    exit;
}

echo 'ERROR';
http_response_code(403);
