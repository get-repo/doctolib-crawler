<?php

namespace Test;

use GetRepo\Doctolib\DoctolibCrawler;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Panther\ProcessManager\WebServerManager;

class DoctolibCrawlerTest extends TestCase
{
    private const HOST = '127.0.0.1';
    private const PORT = 8090;
    private static DoctolibCrawler $doctolibCrawler;
    private static WebServerManager $webServerManager;

    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public static function setUpBeforeClass(): void
    {
        $containerBuilder = null;
        putenv('DOCTOLIB_ENV=test');
        include dirname(__DIR__) . '/bootstrap.php';
        /** @var \Symfony\Component\DependencyInjection\ContainerBuilder $containerBuilder */
        self::$doctolibCrawler = $containerBuilder->get(DoctolibCrawler::class); // @phpstan-ignore-line

        self::$webServerManager = new WebServerManager(
            __DIR__,
            self::HOST,
            self::PORT,
            __DIR__ . '/router.php'
        );
        self::$webServerManager->start();
    }

    public static function tearDownAfterClass(): void
    {
        self::$webServerManager->quit();
    }

    public static function data(): array
    {
        return [
            'Invalid GET parameters' => [
                '2000-01-01',
                0, // in router.php, all ids = 0 means :
                0, // 403 error HTTP response
                0,
                [],
                '[403] ERROR',
            ],
            'invalid date' => [
                '2000-02-02',
                2, // in router.php, same ids means :
                2, // error format in JSON response
                2,
                [],
                '[200] Invalid data : ["INVALID_JSON_FORMAT"]',
            ],
            'no availabilities (reason: no_availabilities)' => [
                '2000-03-03',
                9999, // in router.php, visitMotiveId = 9999 means :
                2,    // no availabilities
                3,
                [],
                'Aucune disponibilité en ligne. (no_availabilities)',
            ],
            'no open dates (reason: not_opened_availability)' => [
                '2000-04-04',
                8888, // in router.php, visitMotiveId = 8888 means :
                3,    // no open dates for booking yet
                4,
                [],
                'Ces dates ne sont pas encore ouvertes à la réservation. (not_opened_availability)',
            ],
            '2 slots found' => [
                '2000-11-30',
                101, // in router.php, agendaId > 100 means :
                201, // found slots
                301,
                [
                    '2000-11-30T06:00:00.000+01:00',
                    '2000-11-30T09:30:00.000+01:00',
                    '2000-12-01T01:00:00.000+01:00',
                    '2000-12-01T03:30:00.000+01:00',
                ],
            ],
        ];
    }

    /**
     * @dataProvider data
     */
    public function testgetSlots(
        string $date,
        int $visitMotiveId,
        int $agendaId,
        int $practiceId,
        array $expectedSlots,
        string $expectedException = null
    ): void {
        if ($expectedException) {
            $this->expectExceptionMessage($expectedException);
        }
        $slots = self::$doctolibCrawler->findSlots(
            new \DateTimeImmutable($date),
            $visitMotiveId,
            $agendaId,
            $practiceId
        );
        $this->assertEquals($expectedSlots, $slots);
    }
}
