#!/usr/bin/env php
<?php
require __DIR__ . '/bootstrap.php';

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;
use GetRepo\Doctolib\DoctolibCrawler;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use GetRepo\Doctolib\DoctolibException;
use Symfony\Component\Yaml\Yaml;
use GetRepo\FreeSmsApi\FreeSmsApiSender;

/** @var array $parameters */
$parameters = Yaml::parseFile(__DIR__ . '/parameters.yml')['parameters'];
/** @var \Symfony\Component\DependencyInjection\ContainerBuilder $containerBuilder */
(new SingleCommandApplication())
    ->addArgument('visit-motive-id', InputArgument::REQUIRED, 'visit_motive_ids GET parameter')
    ->addArgument('agenda-id', InputArgument::REQUIRED, 'agenda_ids GET parameter')
    ->addArgument('practice-id', InputArgument::REQUIRED, 'practice_ids GET parameter')
    ->addOption('name', null, InputOption::VALUE_REQUIRED, 'name')
    ->addOption('days', null, InputOption::VALUE_REQUIRED, 'days (default 30 days)', 30)
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($containerBuilder, $parameters) {
        // show dev mode
        if ($env = getenv('DOCTOLIB_ENV')) {
            $output->writeln(sprintf(
                "<bg=magenta>#### %s MODE ####</>\n",
                strtoupper($env)
            ));
        }

        // command arguments
        $visitMotiveId = (int) $input->getArgument('visit-motive-id');
        $agendaId = (int) $input->getArgument('agenda-id');
        $practiceId = (int) $input->getArgument('practice-id');
        $maxDate = new \DateTimeImmutable(sprintf(
            '+%d day midnight',
            $maxDays = $input->getOption('days')
        ));
        $name = $input->getOption('name');

        // service parameters
        /** @var int $limit */
        /** @psalm-suppress UndefinedDocblockClass */
        $limit = $containerBuilder->getParameter('doctolib.response.max_results');

        // free sms sender
        $freeSmsSender = false;
        if (!$env && array_filter($parameters)) {
            $freeSmsSender = new FreeSmsApiSender(
                $parameters['free_account_id'],
                $parameters['free_account_pwd'],
            );
        }

        // cache already sent results
        /** @var \Symfony\Component\Cache\Adapter\FilesystemAdapter $cache */
        $cache = $containerBuilder->get('doctolib.cache');
        $alreadySentCacheItem = $cache->getItem(sprintf(
            '%s-%d-%d-%d',
            ('test' === $env ? uniqid('sent') : 'sent'),
            $visitMotiveId,
            $agendaId,
            $practiceId,
        ));
        $alreadySent = (array) $alreadySentCacheItem->get();

        // main title
        $output->writeln(sprintf(
            '<bg=gray> %s   %s  %s </> ',
            date('📅 D j F Y'),
            date('🕑 H:i:s'),
            $name ? "👤 {$name}" : null
        ));

        /** @var DoctolibCrawler $crawler */
        $crawler = $containerBuilder->get(DoctolibCrawler::class);

        $maxDateReached = false;
        for ($day = 0; $day <= $maxDays; $day++) {
            if (0 !== ($day % $limit)) {
                continue;
            }
            $date = (new \DateTimeImmutable("+{$day} day"));
            $output->write(sprintf(
                '<fg=blue> %s </> ',
                $date->format('j F Y')
            ));
            try {
                $results = $crawler->findSlots($date, $visitMotiveId, $agendaId, $practiceId);
                // results found
                if ($results) {
                    // filter results
                    $filtered = [];
                    foreach ($results as $result) {
                        $resultDate = new \DateTimeImmutable($result);
                        if ($resultDate >= $maxDate) {
                            $maxDateReached = true;
                            break;
                        }
                        $filtered[] = $resultDate->format('j F Y H:i');
                    }

                    if ($filtered) {
                        $output->writeln(sprintf(
                            '<fg=green>SUCCESS %d RESULTS</>',
                            count($filtered)
                        ));
                        foreach ($filtered as $result) {
                            $slot = (new \DateTimeImmutable($result))->format('j F Y H:i');
                            $output->write($slot);
                            if ($freeSmsSender && !isset($alreadySent[$slot])) {
                                $freeSmsSender->send(<<<SMS
[DOCTOLIB]
{$name}
{$slot}
SMS
                                );

                                $alreadySent[$slot] = sprintf(
                                    'sent the %s at %s',
                                    date('Y-m-d'),
                                    date('H:i:s')
                                );
                                $alreadySentCacheItem->set($alreadySent);
                                $cache->save($alreadySentCacheItem);
                                $output->writeln(" <fg=green>SMS SENT</>");

                                return 0; // stop script
                            }
                            $output->writeln('');
                        }
                    } else {
                        $output->writeln("no results");
                    }
                } else {
                    $output->writeln("no results");
                }
                $output->writeln('');
            } catch (DoctolibException $e) {
                $output->writeln(sprintf(
                    '<error>%s</>',
                    $e->getMessage()
                ));
            }
            if ($maxDateReached) {
                break;
            }
        }
    })
    ->run();
