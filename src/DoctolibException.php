<?php

namespace GetRepo\Doctolib;

class DoctolibException extends \Exception
{
    public function __construct(string $reason, string $message)
    {
        parent::__construct(sprintf('%s (%s)', $message, $reason), 400);
    }
}
