<?php

namespace GetRepo\Doctolib;

use Symfony\Component\Cache\Adapter\AdapterInterface;

class DoctolibCrawler
{
    public function __construct(
        private readonly AdapterInterface $cache,
        private readonly array $headers,
        private readonly int $limit,
    ) {
    }

    public function findSlots(
        \DateTimeImmutable $date,
        int $visitMotiveId,
        int $agendaId,
        int $practiceId
    ): array {
        $env = getenv('DOCTOLIB_ENV');
        $url = sprintf(
            // phpcs:ignore
            '%s/availabilities.json?start_date=%s&visit_motive_ids=%s&agenda_ids=%s&practice_ids=%s&insurance_sector=public&limit=%d',
            ('test' === $env ? 'http://127.0.0.1:8090' : 'https://www.doctolib.fr'),
            $date->format('Y-m-d'),
            $visitMotiveId,
            $agendaId,
            $practiceId,
            $this->limit
        );

        $urlCacheItem = $this->cache->getItem($url);
        if ('dev' === $env && $urlCacheItem->isHit()) {
            return $urlCacheItem->get();
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception('CURL error ' . curl_error($ch));
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($json = @json_decode((string) $content, true)) {
            // reason is set to there is no slots
            if (isset($json['reason'])) {
                throw new DoctolibException($json['reason'], $json['message'] ?? 'Aucune disponibilité');
            }
            $availabilities = $json['availabilities'] ?? [];
            if (is_array($availabilities) && $availabilities) {
                $results = [];
                array_walk_recursive($availabilities, function ($data) use (&$results) {
                    if (is_string($data) && preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/', $data)) {
                        $results[] = $data;
                    }
                });

                if ('dev' === $env) {
                    $urlCacheItem->set($results);
                    $this->cache->save($urlCacheItem);
                }

                return $results;
            }
            throw new \Exception("[{$info['http_code']}] Invalid data : {$content}");
        }

        throw new \Exception("[{$info['http_code']}] {$content}");
    }
}
